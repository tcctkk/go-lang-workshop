package services

import (
	context "context"
	"database/sql"

	"server/errs"
	"server/logs"
	"server/repository"

	"go.uber.org/zap"
)

type customerServer struct {
	custRepo repository.CustomerRepository
}

func NewCustomerServer(custRepo repository.CustomerRepository) CustomerServer {
	return customerServer{custRepo}
}

func (customerServer) mustEmbedUnimplementedCustomerServer() {}


func (s customerServer) GetCustomers(req *Empty, stream Customer_GetCustomersServer) error {
    customers, err := s.custRepo.GetCustomers()
    if err != nil {
        logs.Error(err)
        return err
    }

    for _, customer := range customers {
        custRecord := &CustomerRecord{
            Name:          customer.CustomerName,
            PhoneNumber:   customer.PhoneNumber,
            BirthDate:     customer.BirthDate,
            CustomerId:    customer.CustomerID,
        }

        if err := stream.Send(custRecord); err != nil {
            logs.Error(err)
            return err
        }
    }

    logs.Info("Get Customers", zap.Any("customers", customers))
    return nil
}

func (s customerServer) GetCustomer(ctx context.Context, req *Id) (*CustomerRecord, error){
	customer, err := s.custRepo.GetCustomer(req.CustomerId)
	logs.Info("Get Customer ByID : ", zap.String("id", req.CustomerId))

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotfoundError("Customer Not Found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	custRecord := CustomerRecord{
		Name:          customer.CustomerName,
		PhoneNumber:   customer.PhoneNumber,
		BirthDate:     customer.BirthDate,
		CustomerId:    customer.CustomerID,
	}

	logs.Info("Get Customer", zap.String("id", req.CustomerId), zap.Any("customers", &custRecord))
	return &custRecord, nil
}

func (s customerServer) InsertCustomer(ctx context.Context, req *CustomerRecord) (*Id, error) {
    customer := repository.Customer{
        CustomerID:   req.CustomerId,
        CustomerName: req.Name,
        PhoneNumber:  req.PhoneNumber,
        BirthDate:    req.BirthDate,
    }

    customerID, err := s.custRepo.InsertCustomer(customer)
    if err != nil {
        logs.Error(err)
        return nil, errs.NewValidationError("Validation Error")
    }
    
    id := Id{CustomerId: customerID,}
    logs.Info("Insert Customer: ", zap.String("id", customerID))
    return &id, nil
}

func (s customerServer) UpdateCustomer(ctx context.Context, req *CustomerRecord) (*Status, error){
	customer := repository.Customer{
		CustomerID:   req.CustomerId,
		CustomerName: req.Name,
		PhoneNumber:  req.PhoneNumber,
		BirthDate:    req.BirthDate,
	}

	customerID, err := s.custRepo.UpdateCustomer(customer)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewValidationError("Validation Error")
	}
	
	status := Status{Value: "Update Customer Success",}
	logs.Info("Update Customer  : ", zap.String("id", customerID))
	return &status, nil
}

func (s customerServer) DeleteCustomer(ctx context.Context, req *Id) (*Status, error) {
	err := s.custRepo.RemoveCustomer(req.CustomerId)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewNotfoundError("Customer Not Found")
	}
	status := Status{Value: "Remove Customer Success",}
	logs.Info("Remove Customer  : ", zap.String("id", req.CustomerId))
    return &status, nil
}
