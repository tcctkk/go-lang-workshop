package repository

type Customer struct {
	CustomerID   string `gorm:"column:customer_id" json:"Customer_id" `
	CustomerName string `json:"Customer_name"`
	PhoneNumber  string `json:"Phone_number"`
	BirthDate    string `json:"Birth_date"`
}

type CustomerRepository interface {
	GetCustomers() ([]Customer, error)
	GetCustomer(string) (*Customer, error)
	InsertCustomer(Customer) (string, error)
	RemoveCustomer(string) error
	UpdateCustomer(Customer) (string, error)
}
