package main

import "fmt"

func multiplyTable(n int) {
	for i := 1; i < n+1; i++ {
		for j := 1; j < n+1; j++ {
			multiplyNumber := i * j
			fmt.Print(multiplyNumber, "\t")
		}
		fmt.Println()
	}
}
func main() {
	multiplyTable(12)
}
