package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func getName() string {
	fmt.Print("Enter your name : ")
	name, _ := reader.ReadString('\n')
	return strings.TrimSpace(name)
}

func main() {
	name := getName()
	fmt.Printf("Hello, %v !", name)
}
