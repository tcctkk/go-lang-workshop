package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func getValue(promt string) float64 {
	fmt.Printf("%v", promt)
	input, _ := reader.ReadString('\n')
	value, err := strconv.ParseFloat(strings.TrimSpace(input), 64)
	if err != nil {
		message, _ := fmt.Printf("%v must be number only", promt)
		panic(message)
	}
	return value
}

func findPrimeNum(value float64) string {
	var result string
	count := int((value / 2) + 1)
	for i := 0; i < count; i++ {
		temp := int(math.Mod(value, float64(i)))
		if temp == 0 || value == 0 {
			result = "is not a prime number"
		} else if temp != 0 {
			result = "is a prime number"
		}
	}
	// fmt.Println(result)
	return result
}

func main() {
	value := getValue("input = ")
	result := findPrimeNum(value)
	fmt.Printf("%[1]v %[2]v\n", value, result)
}
