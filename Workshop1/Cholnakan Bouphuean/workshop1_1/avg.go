package main

import "fmt"

func avg(arr []float64, size int) float64 {
	var avg, sum float64
	for i := 0; i < size; i++ {
		sum += arr[i]
	}
	avg = sum / float64(size)
	return avg
}

func main() {
	num := []float64{1.5, 2.0, 3.5, 4.0, 5.5, 6.0, 7.5}
	result := avg(num, len(num))
	fmt.Println("result = ", result)

}
