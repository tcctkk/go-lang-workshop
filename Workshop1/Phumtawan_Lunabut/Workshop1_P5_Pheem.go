package main

import "fmt"

func gcd(a, b int) int {
	if b == 0 {
		return a
	}
	return gcd(b, a%b)
}

func lcm(a, b int) int {
	return a * b / gcd(a, b)
}

func lcmList(list []int) int {
	result := list[0]
	for i := 1; i < len(list); i++ {
		result = lcm(result, list[i])
	}
	return result
}

func main() {
	list := []int{2, 4, 6, 8, 10}
	result := lcmList(list)
	fmt.Println("LCM of list", list, "is", result)
}
