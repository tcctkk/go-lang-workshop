package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var Db *sql.DB

//var courseList []Course

const productPath = "products"
const basePath = "/api"

type Product struct {
	ProductID     int     `json: "productid"`
	Productname   string  `json: "productname"`
	Price         float64 `json: "price"`
	Productdetail string  `json: "productdetail"`
	Datecreated   string  `json: "datecreated"`
}

func SetupDB() {
	var err error
	Db, err = sql.Open("mysql", "root:27926@tcp(127.0.0.1:3306)/godb")

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(Db)
	Db.SetConnMaxLifetime(time.Minute * 3)
	Db.SetMaxOpenConns(10)
	Db.SetMaxIdleConns(10)

}

func getProductList() ([]Product, error) {
	//เกิน 3 วิปิดการทำงาน cancel()
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	results, err := Db.QueryContext(ctx, `SELECT 
	productid,
	productname,
	price,
	productdetail,
	datecreated
	FROM product`)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	defer results.Close()
	products := make([]Product, 0)
	for results.Next() {
		var product Product
		results.Scan(&product.ProductID,
			&product.Productname,
			&product.Price,
			&product.Productdetail,
			&product.Datecreated)

		products = append(products, product)
	}
	return products, nil

}

func insertProduct(product Product) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	datecreated := time.Now()
	result, err := Db.ExecContext(ctx, `INSERT INTO product
	(productid,
	productname,
	price,
	productdetail,
	datecreated
	) VALUES (?,?,?,?,?)`,
		product.ProductID,
		product.Productname,
		product.Price,
		product.Productdetail,
		datecreated)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	insertID, err := result.LastInsertId()
	if err != nil {
		log.Print(err.Error())
		return 0, err
	}
	return int(insertID), nil
}

func getProduct(productid int) (*Product, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	row := Db.QueryRowContext(ctx, `SELECT
	productid,
	productname,
	price,
	productdetail,
	datecreated
	FROM product
	WHERE productid = ?`, productid)

	product := &Product{}
	err := row.Scan(
		&product.ProductID,
		&product.Productname,
		&product.Price,
		&product.Productdetail,
		&product.Datecreated)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		log.Print(err)
		return nil, err
	}
	return product, nil
}

func removeProduct(productID int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err := Db.ExecContext(ctx, `DELETE FROM product where productid = ?`, productID)
	if err != nil {
		log.Print(err.Error())
		return err
	}
	return nil
}

func handleProducts(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		productList, err := getProductList()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		j, err := json.Marshal(productList)
		log.Println("")
		if err != nil {
			log.Fatal(err)
		}
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Request API : %s Calling MethodGET to show data from database : %v", r.URL.Path, productList)
	case http.MethodPost:
		var product Product
		// read body
		err := json.NewDecoder(r.Body).Decode(&product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		ProductID, err := insertProduct(product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"productid":%d}`, ProductID)))
		log.Printf("Request API : %s Calling MethodPOST to Add data to database : %v", r.URL.Path, product)
	case http.MethodOptions:
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// handler for get single data
func handleProduct(w http.ResponseWriter, r *http.Request) {
	urlPathSegments := strings.Split(r.URL.Path, fmt.Sprintf("%s/", productPath))
	if len(urlPathSegments[1:]) > 1 {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	productID, err := strconv.Atoi(urlPathSegments[len(urlPathSegments)-1])
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	switch r.Method {
	case http.MethodGet:
		product, err := getProduct(productID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if product == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		j, err := json.Marshal(product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Request API : %s Calling MethodGET to show data from database with Id : %v", r.URL.Path, product)

	case http.MethodDelete:
		product, err := getProduct(productID)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		error := removeProduct(productID)
		if error != nil {
			log.Print(error)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		log.Printf("Request API : %s Calling MethodDelete to Delete data from database with Id : %v", r.URL.Path, product)
	case http.MethodPut:
		product, err := getProduct(productID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if product == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		defer r.Body.Close()
		log.Printf("Request API : %s Calling MethodPut to Update data from database with Id : %v", r.URL.Path, product)

		var updatedProduct Product
		err = json.Unmarshal(body, &updatedProduct)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if updatedProduct.ProductID != productID {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		err = updateProduct(updatedProduct)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func updateProduct(product Product) error {
	ctx, cancle := context.WithTimeout(context.Background(), time.Second*3)
	defer cancle()
	_, err := Db.ExecContext(ctx, `UPDATE product 
	SET 
		productname = ?,
		price = ?, 
		productdetail = ?,
		datecreated = ?
		WHERE productid = ?`,
		product.Productname,
		product.Price, product.Productdetail,
		product.Datecreated,
		product.ProductID)

	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

// middleware
func corsMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//log.Printf("Incoming request: %s %s", r.Method, r.URL.Path)
		// if r.Method == "GET" {
		// 	log.Printf("calling method get for fetch data from database")
		// }
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Add("Access-Control-Allow-Methods", "POST,GET,OPTIONS,PUT,DELETE")
		w.Header().Add("Access-Control-Allow-Header", "Accept,Content-Type,Content-Lenght,Authorization, X-C")

		handler.ServeHTTP(w, r)

		//Log the outgoing response
		//log.Printf("Outgoing response: %s", w.Header().Get("Content-Type"))

	})
}

func SetupRoutes(apiBasePath string) {
	productsHandler := http.HandlerFunc(handleProducts)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, productPath), corsMiddleware(productsHandler))
	productHandler := http.HandlerFunc(handleProduct)
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, productPath), corsMiddleware(productHandler))

}

func main() {
	SetupDB()
	// cinfig api
	SetupRoutes(basePath)
	// config port
	log.Fatal(http.ListenAndServe(":5000", nil))
}
