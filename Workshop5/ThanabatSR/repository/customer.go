package repository

type Customer struct {
	CustomerID  int    `gorm:"customer_id"`
	Name        string `gorm:"name"`
	DateCreate  string `gorm:"date_create"`
	PhoneNumber string  `gorm:"phone_number"`
}

type CustomerRepository interface {
    GetAll() ([]Customer, error)
    GetById(id int) (*Customer, error)
    DeleteCustomer(id int) error
    InsertCustomer(c Customer) (*Customer, error)
    UpdateCustomer(id int, c Customer) (*Customer, error)
}
