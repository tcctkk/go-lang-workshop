package service

import (
	"database/sql"
	"fmt"
	"hexa_pheem/errs"
	"hexa_pheem/logs"
	"hexa_pheem/repository"
	"time"
)

type productService struct {
	prodRepo repository.ProductRepository
}

func NewProductService(prodRepo repository.ProductRepository) ProductService {
	return productService{prodRepo: prodRepo}
}

func (s productService) NewProduct(productID int, request NewProductRequest) (*ProductResponse, error) {
	product := repository.Product{
		ID:             productID,
		Product_Name:   request.Product_Name,
		Price:          request.Price,
		Product_Detail: request.Product_Detail,
		Date_Created:   time.Now().Format("2006-1-2 15:04:05"),
	}
	newProd, err := s.prodRepo.Create(product)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	response := ProductResponse{
		ID:             productID,
		Product_Name:   newProd.Product_Name,
		Price:          newProd.Price,
		Product_Detail: newProd.Product_Detail,
		Date_Created:   newProd.Date_Created,
	}
	return &response, nil
}

func (s productService) GetProducts(productID int) (*ProductResponse, error) {
	products, err := s.prodRepo.GetByID(productID)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("Here")
			return nil, errs.NewNotFoundError("product not found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	response := ProductResponse{
		ID:             products.ID,
		Product_Name:   products.Product_Name,
		Price:          products.Price,
		Product_Detail: products.Product_Detail,
		Date_Created:   products.Date_Created,
	}
			
	return &response, nil
}
func (s productService) GetAllProducts() ([]ProductResponse, error) {
	products, err := s.prodRepo.GetAllProduct()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	response := []ProductResponse{}
	for _, product := range products {
		response = append(response, ProductResponse{
			ID:             product.ID,
			Product_Name:   product.Product_Name,
			Price:          product.Price,
			Product_Detail: product.Product_Detail,
			Date_Created:   product.Date_Created,
		})
	}
	return response, nil
}

func (s productService) UpdateProduct(productID int, request NewProductRequest) (*ProductResponse,error) {
	product := repository.Product{
		ID:             productID,
		Product_Name:   request.Product_Name,
		Price:          request.Price,
		Product_Detail: request.Product_Detail,
	}
	prod,err := s.prodRepo.Update(productID, product)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	response := ProductResponse{
		ID:             prod.ID,
		Product_Name:   prod.Product_Name,
		Price:          prod.Price,
		Product_Detail: prod.Product_Detail,
		Date_Created:   prod.Date_Created,
	}
	return &response,nil
}
func (s productService) DeleteProduct(productID int) error {

	err := s.prodRepo.Delete(productID)
	if err != nil {
		logs.Error(err)
		return errs.NewUnexpectedError()
	}
	return nil
}
