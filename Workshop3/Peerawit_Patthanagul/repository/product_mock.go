package repository

import "errors"

type ProductRepositoryMock struct {
	products []Product
}

func NewProductRepositoryMock() ProductRepositoryMock {
	products := []Product{
		{ID: 1, Product_Name: "Glock-17", Price: 36900, Product_Detail: "Ammo Velocity 220m/s"},
	}
	return ProductRepositoryMock{products: products}

}
func (r ProductRepositoryMock) GetAll() ([]Product, error) {
	return r.products, nil
}
func (r ProductRepositoryMock) GetById(id int) (*Product, error) {
	for _, product := range r.products {
		if product.ID == id {
			return &product, nil
		}
	}
	return nil, errors.New("Customer not found!!")
}