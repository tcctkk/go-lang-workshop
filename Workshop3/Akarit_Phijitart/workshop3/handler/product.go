package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/Akarit2001/workshop3/service"
	"github.com/gorilla/mux"
)

const appjson = "application/json"
const contype = "content-type"

type productHandler struct {
	productService service.ProductService
}

func NewProductHandler(productService service.ProductService) productHandler {
	return productHandler{productService: productService}
}

func (h productHandler) GetProducts(w http.ResponseWriter, r *http.Request) {
	products, err := h.productService.GetProducts()
	if err != nil {
		handleErr(w, err)
		return
	}
	w.Header().Set(contype, appjson)
	json.NewEncoder(w).Encode(products)
}

func (h productHandler) GetProduct(w http.ResponseWriter, r *http.Request) {
	pid, _ := strconv.Atoi(mux.Vars(r)["pid"])
	product, err := h.productService.GetProduct(pid)
	if err != nil {
		handleErr(w, err)
		return
	}
	w.Header().Set(contype, appjson)
	json.NewEncoder(w).Encode(product)
}

func (h productHandler) AddNewProduct(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get(contype) != appjson {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "data not json")
		return
	}
	request := service.ProductResquest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	response, err := h.productService.AddNewProduct(request)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set(contype, appjson)
	json.NewEncoder(w).Encode(response)
}

func (h productHandler) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	pid, _ := strconv.Atoi(mux.Vars(r)["pid"])
	if r.Header.Get(contype) != appjson {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "data not json")
		return
	}
	request := service.ProductResquest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	response, err := h.productService.UpdateProduct(pid, request)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set(contype, appjson)
	json.NewEncoder(w).Encode(response)
}

func (h productHandler) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	pid, _ := strconv.Atoi(mux.Vars(r)["pid"])
	err := h.productService.DeleteProduct(pid)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	fmt.Fprintln(w, "deleted")
}
