package repository

import (
	"context"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
)

type productRepositroyDB struct {
	db *sqlx.DB
}

func NewProductRepositroy(db *sqlx.DB) productRepositroyDB { //อันนี้ึคือ constructor
	return productRepositroyDB{db: db}
}

func (r productRepositroyDB) GetAll() ([]Product, error) {
	product := []Product{}
	query := "select id, product_name, price,product_detail, date_created from product"
	err := r.db.Select(&product, query)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (r productRepositroyDB) GetById(id int) (*Product, error) {
	product := Product{}
	query := "select id, product_name, price,product_detail, date_created from product where id=?"
	err := r.db.Get(&product, query, id)
	if err != nil {
		return nil, err
	}
	return &product, nil // ใช้ & เพื่อ return ตำแหน่งออกไป
}

func (r productRepositroyDB) Insert(p Product) error {
	query := "insert into product(product_name, price, product_detail, date_created) values(?, ?, ?,?)"
	_, err := r.db.Exec(query, p.Name, p.Price, p.ProductDetail, p.DateCreate)
	if err != nil {
		return err
	}
	return nil
}

func (r productRepositroyDB) Update(productId int, p Product) error {
	query := "update product set product_name=?, price=?, product_detail=?, date_created=? where id=?"
	_, err := r.db.Exec(query, p.Name, p.Price, p.ProductDetail, p.DateCreate, productId)
	if err != nil {
		return err
	}
	return nil
}

func (r productRepositroyDB) Delete(productId int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err := r.db.ExecContext(ctx, `DELETE FROM product WHERE id = ?`, productId)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}
