package main

import (
	"bank/handler"
	"bank/logs"
	"bank/repository"
	"bank/service"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

func main() {
	initTimeZone()
	initConfig()
	db := initDatabase()

	productRepositoryDB := repository.NewProductRepositroy(db)
	ProductService := service.NewProductService(productRepositoryDB)
	productHandler := handler.NewProductHandler(ProductService)

	router := mux.NewRouter()
	router.Use(corsMiddleware)

	router.HandleFunc("/products", productHandler.GetProducts).Methods(http.MethodGet)
	router.HandleFunc("/product", productHandler.AddProduct).Methods(http.MethodPost)
	router.HandleFunc("/product/{productID:[0-9]+}", productHandler.GetProduct).Methods(http.MethodGet)
	router.HandleFunc("/product/{productID}", productHandler.UpdateProduct).Methods(http.MethodPut)
	router.HandleFunc("/product/{productID:[0-9]+}", productHandler.DeleteProduct).Methods(http.MethodDelete)

	log.Printf("Product service start at port %v", viper.GetInt("app.port"))
	//logs.Info("Product service start at port" + viper.GetString("app.port"))

	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt("app.port")), router)
}

func corsMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Authorization")

		rec := httptest.NewRecorder()
		handler.ServeHTTP(rec, r)
		statusCode := rec.Code

		// log request method, URL path, and status code
		port := strconv.Itoa(viper.GetInt("app.port"))
		logs.Info(fmt.Sprintf("INFO - %s http://localhost:%s%s | status: %d", r.Method, port, r.URL.Path, statusCode))

		if r.Method == "GET" {
			payload, err := ioutil.ReadAll(rec.Body)
			if err != nil {
				logs.Error("failed to read response payload")
			} else {
				payloadStr := string(payload)
				log.Printf("%s", payloadStr)
			}
		}
		if r.Method == http.MethodPut {
			payload, err := ioutil.ReadAll(rec.Body)
			if err != nil {
				logs.Error("failed to read response payload")
			} else {
				payloadStr := string(payload)
				log.Printf("%s", payloadStr)
			}
		}

		for k, v := range rec.Header() {
			w.Header()[k] = v
		}
		w.WriteHeader(statusCode)
		rec.Body.WriteTo(w)
		handler.ServeHTTP(w, r)
	})
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}

func initDatabase() *sqlx.DB {
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetInt("db.port"),
		viper.GetString("db.database"),
	)
	db, err := sqlx.Open(viper.GetString("db.driver"), dsn)
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(3 * time.Minute)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db
}
