package repository

//Customer_id, Customer_name, Phone_number, Date_created
type Customer struct {
	CustomerID   int    `db:"Customer_id"`
	CustomerName string `db:"Customer_name"`
	PhoneNumber  string `db:"Phone_number"`
	DateCreated  string `db:"Date_created"`
}

type CustomerRepository interface {
	GetAll() ([]Customer, error)
	GetById(int) (*Customer, error)
	InsertCustomer(Customer) (int, error)
	RemoveCustomer(int) error
	UpdateCustomer(Customer) (int, error)
}
