package repository

type Product struct {
	ProductID     int     `db:"product_id"`
	ProductName   string  `db:"product_name"`
	Price         float32 `db:"price"`
	ProductDetail string  `db:"product_detail"`
	DateCreated   string  `db:"date_created"`
}
type ProductRepository interface {
	Create(Product) (*Product, error)
	Update(Product) (*Product, error)
	Delete(int) error
	GetAll() ([]Product, error)
	GetById(int) (*Product, error)
}
