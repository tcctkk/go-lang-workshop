package handler

import (
	"encoding/json"
	"fmt"
	"hexa_arh/errs"
	"hexa_arh/repository"
	"hexa_arh/service"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type customerHandler struct {
	customerService service.CustomerService
}

func NewCustomerHandler(customerService service.CustomerService) customerHandler {
	return customerHandler{customerService: customerService}
}

func (h customerHandler) GetCustomers(w http.ResponseWriter, r *http.Request) {
	customers, err := h.customerService.GETCustomers()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	json_, err := json.Marshal(customers)
	if err != nil {
		log.Fatal(err)
	}
	_, err = w.Write(json_)
	if err != nil {
		log.Fatal(err)
	}
}

func (h customerHandler) GetCustomer(w http.ResponseWriter, r *http.Request) {
	fmt.Println(0)
	customerid, _ := strconv.Atoi(mux.Vars(r)["customerID"])
	fmt.Println(customerid)

	customer, err := h.customerService.GETCustomer(customerid)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		fmt.Fprintln(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(customer)
}

func (h customerHandler) AddCustomer(w http.ResponseWriter, r *http.Request) {
	var customer repository.Customer
	err := json.NewDecoder(r.Body).Decode(&customer)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		fmt.Fprintln(w, err)
		return
	}

	customerid, err := h.customerService.ADDCustomer(customer)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		fmt.Fprintln(w, err)
		return
	}
	fmt.Println(customerid)

}

func (h customerHandler) DeleteCustomer(w http.ResponseWriter, r *http.Request) {
	customerid, _ := strconv.Atoi(mux.Vars(r)["customerID"])
	fmt.Println(customerid)

	err := h.customerService.DELETECustomer(customerid)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		fmt.Fprintln(w, err)
		return
	}
	fmt.Println(customerid)
}

func (h customerHandler) UpdateCustomer(w http.ResponseWriter, r *http.Request) {
	customerid, _ := strconv.Atoi(mux.Vars(r)["customerID"])
	var customer repository.Customer
	err := json.NewDecoder(r.Body).Decode(&customer)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		fmt.Fprintln(w, err)
		return
	}
	customerid_, err := h.customerService.UPDATECustomer(customer, customerid)
	if err != nil {
		appErr, ok := err.(errs.AppError)
		if ok {
			w.WriteHeader(appErr.Code)
			fmt.Fprintln(w, appErr.Message)
		}
	} else {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, "Update successfully")
	}
	_ = customerid_
}
