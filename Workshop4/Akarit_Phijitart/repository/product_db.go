package repository

import (
	"github.com/jmoiron/sqlx"
)

type productRepositoryDB struct {
	db *sqlx.DB
}

func NewProductRepositoryDB(db *sqlx.DB) productRepositoryDB {
	return productRepositoryDB{db: db}
}

func (r productRepositoryDB) GetAll() ([]Product, error) {
	products := []Product{}
	query := "SELECT id, product_name, price, product_detail, date_created FROM tcc_workshop.product"
	err := r.db.Select(&products, query)
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (r productRepositoryDB) GetById(id int) (*Product, error) {

	query := "SELECT id, product_name, price, product_detail, date_created FROM tcc_workshop.product WHERE id = ?"

	product := Product{}
	err := r.db.Get(&product, query, id)
	if err != nil {
		return nil, err
	}
	return &product, nil
}

func (r productRepositoryDB) Create(product Product) (*Product, error) {
	query := "INSERT INTO `tcc_workshop`.`product` (`product_name`, `price`, `product_detail`, `date_created`) VALUES (?, ?, ?, ?);"

	result, err := r.db.Exec(query,
		product.ProductName,
		product.Price,
		product.ProductDetail,
		product.DateCreated,
	)
	if err != nil {
		return nil, err
	}
	pid, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}
	product.ID = int(pid)
	return &product, nil
}

func (r productRepositoryDB) Update(product Product) (*Product, error) {
	query := "UPDATE `tcc_workshop`.`product` SET `product_name` = ?, `price` = ?, `product_detail` = ? WHERE (`id` = ?);"

	_, err := r.db.Exec(query,
		product.ProductName,
		product.Price,
		product.ProductDetail,
		product.ID,
	)

	if err != nil {
		return nil, err
	}

	selectQuery := "SELECT `id`, `product_name`, `price`, `product_detail`, `date_created` FROM `tcc_workshop`.`product` WHERE (`id` = ?);"

	updatedProduct := Product{}
	err = r.db.Get(&updatedProduct, selectQuery, product.ID)
	if err != nil {
		return nil, err
	}
	return &updatedProduct, nil
}

func (r productRepositoryDB) Delete(pid int) error {
	query := "DELETE FROM `tcc_workshop`.`product` WHERE (`id` = ?);"

	_, err := r.db.Exec(query, pid)

	if err != nil {
		return err
	}

	return nil
}
