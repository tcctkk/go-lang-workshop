package repository

import "time"

type Product struct {
	ID            int       `db:"id"`
	ProductName   string    `db:"product_name"`
	Price         float64   `db:"price"`
	ProductDetail string    `db:"product_detail"`
	DateCreated   time.Time `db:"date_created"`
}

type ProductRepository interface {
	GetAll() ([]Product, error)
	GetById(int) (*Product, error)
	Create(Product) (*Product, error)
	Update(Product) (*Product, error)
	Delete(int) error
}
