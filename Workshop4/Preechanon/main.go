package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"

	"fiber_framework/handler"
	"fiber_framework/repository"
	"fiber_framework/service"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func apiDb() {

	initConfig()
	initTimeZone()
	db := initDatabase()

	customerRepository := repository.NewCustomerRepositoryDB(db)
	customerService := service.NewCustomerService(customerRepository)
	customerHandler := handler.NewCustomerHandler(customerService)

	app := fiber.New()

	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*,http://localhost:5173",
		AllowMethods:     "POST, GET, OPTIONS, PUT, DELETE",
		AllowHeaders:     "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token",
		AllowCredentials: true,
	}))
	/*app.Use(func(c *fiber.Ctx) error {
		c.Set("Access-Control-Allow-Origin", "*,http://localhost:5173")
		c.Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		c.Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token")
		c.Set("Content-Type", "application/json")
		return c.Next()
	})*/

	app.Get("/customers", customerHandler.GetCustomers)
	app.Get("/customers/:id", customerHandler.GetCustomer)
	app.Post("/customers", customerHandler.AddCustomer)
	app.Put("/customers/:id", customerHandler.UpdateCustomer)
	app.Delete("/customers/:id", customerHandler.DeleteCustomer)

	err := app.Listen(fmt.Sprintf(":%v", viper.GetInt("app.port")))
	if err != nil {
		log.Fatal(err)
	}

}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {

	apiDb()
}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}

func initDatabase() *sqlx.DB {
	db, err := sqlx.Open(viper.GetString("db.driver"), fmt.Sprintf("%v@tcp(%v:%v)/%v",
		viper.GetString("db.username"),
		viper.GetString("db.host"),
		viper.GetString("db.port"),
		viper.GetString("db.database")))
	if err != nil {
		panic(err)
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db
}
