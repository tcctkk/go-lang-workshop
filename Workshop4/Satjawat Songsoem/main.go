package main

import (
	"fmt"
	"gofiber/handler"
	"gofiber/repository"
	"gofiber/service"
	"log"
	"time"
	"github.com/gofiber/fiber/v2/middleware/cors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)


var db *sqlx.DB

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func SetupDB() *sqlx.DB {
	var err error
	initConfig()
	db, err := sqlx.Open(viper.GetString("db.driver"), fmt.Sprintf("%v:%v@tcp(%v:%v)/%v",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetInt("db.port"),
		viper.GetString("db.database"),
	))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(db)
	db.SetConnMaxIdleTime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db
}

func main() {
	db = SetupDB()
	app := fiber.New()
	
	app.Use(cors.New(cors.Config{
        AllowOrigins: "*",
        AllowMethods: "GET,POST,PUT,DELETE",
        AllowHeaders: "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token",
    }))


	customerRepositoryDB := repository.NewCustomerRepositoryDB(db)
	customerService := service.NewCustomerService(customerRepositoryDB)
	customerHandler := handler.NewCustomerHandler(customerService)
	
	app.Get("/customer/:customerID", customerHandler.GetCustomer)
	app.Get("/customer", customerHandler.GetCustomers)
	app.Post("/customer", customerHandler.InsertCustomer)
	app.Put("/customer", customerHandler.UpdateCustomer)
	app.Delete("/customer/:customerID", customerHandler.RemoveCustomer)

	app.Listen(":8000")
}

