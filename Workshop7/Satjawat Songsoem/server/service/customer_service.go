package service

type CustomerResponse struct {
	CustomerID   int    `gorm:"column:id" json:"Customer_id"`
	CustomerName string `json:"Customer_name"`
	PhoneNumber  string `json:"Phone_number"`
	BirthDate    string `json:"Birth_date"`
}

type CustomerService interface {
	GetCustomers() ([]CustomerResponse, error)
	/*
	GetCustomer(int) (*CustomerResponse, error)
	InsertCustomer(CustomerResponse) (int, error)
	RemoveCustomer(int) error
	UpdateCustomer(CustomerResponse) (int, error)
	*/
}
