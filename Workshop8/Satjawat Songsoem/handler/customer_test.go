package handler_test

import (
	"encoding/json"
	"errors"
	
	"gofiber/handler"
	"gofiber/repository"
	"gofiber/service"
	"io"
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
)

func TestGetCustomer(t *testing.T) {
	
	t.Run("success", func(t *testing.T) {
		
		custRepo := repository.NewCustomerRepositoryMock()
		expectedCustomers := []repository.Customer{
			{
				CustomerID:   1,
				CustomerName: "Satjawat",
				PhoneNumber:  "09888888888",
				BirthDate:    "22/12/2001",
			},
			{
				CustomerID:   2,
				CustomerName: "Copter",
				PhoneNumber:  "0984948286",
				BirthDate:    "15/12/2001",
			},
		}
		custRepo.On("GetCustomers").Return(expectedCustomers, nil)

		
		custService := service.NewCustomerService(custRepo)
		custHandler := handler.NewCustomerHandler(custService)

		
		app := fiber.New()
		app.Get("/customer", custHandler.GetCustomers)

		
		req := httptest.NewRequest("GET", "/customer", nil)

		
		res, err := app.Test(req)
		assert.NoError(t, err)
		defer res.Body.Close()

		assert.Equal(t, fiber.StatusOK, res.StatusCode)
		assert.Equal(t, "application/json", res.Header.Get("Content-Type"))

		expectedBody, _ := json.Marshal(expectedCustomers)
		body, _ := io.ReadAll(res.Body)
		assert.JSONEq(t, string(expectedBody), string(body))
	})


	t.Run("error", func(t *testing.T) {
		custRepo := repository.NewCustomerRepositoryMock()
		expectedErr := errors.New("error retrieving customers")
		custRepo.On("GetCustomers").Return([]repository.Customer{}, expectedErr)
	
		custService := service.NewCustomerService(custRepo)
		custHandler := handler.NewCustomerHandler(custService)
	
		app := fiber.New()
		app.Get("/customer", custHandler.GetCustomers)
	
		req := httptest.NewRequest("GET", "/customer", nil)
	
		res, err := app.Test(req)
		assert.NoError(t, err)
		defer res.Body.Close()
	
		assert.Equal(t, fiber.StatusInternalServerError, res.StatusCode)
	
		expectedBody := "Error: unexpected error"
		body, _ := io.ReadAll(res.Body)

		assert.Equal(t, string(expectedBody), string(body))
	})
}
