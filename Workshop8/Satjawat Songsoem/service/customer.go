package service

import (
	"gofiber/errs"
	"gofiber/logs"
	"gofiber/repository"

	"go.uber.org/zap"
)

type customerService struct {
	custRepo repository.CustomerRepository
}

func NewCustomerService(custRepo repository.CustomerRepository) CustomerService {
	return customerService{custRepo}
}

func (s customerService) GetCustomers() ([]CustomerResponse, error) {
	customers, err := s.custRepo.GetCustomers()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	custResponses := []CustomerResponse{}
	for _, customer := range customers {
		custResponse := CustomerResponse{
			CustomerID:   customer.CustomerID,
			CustomerName: customer.CustomerName,
			PhoneNumber:  customer.PhoneNumber,
			BirthDate:    customer.BirthDate,
		}
		custResponses = append(custResponses, custResponse)
	}
	logs.Info("Get Customers", zap.Any("customers", custResponses))
	return custResponses, nil
}
